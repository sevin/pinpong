#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#define SNAME_ping "/mysem"
#define SNAME_pong "/mysem2"

struct s_data {
    long counter;
    struct timespec start;
} data;

struct timespec diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

void signal_callback_handler(int signum)
{
   // Terminate program
   struct timespec time1;
   clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
   // Terminate program
   struct timespec difference = diff(data.start, time1);

   printf("Time taken %ld seconds %ld nanoseconds (%ld iterations)\n", 
   difference.tv_sec, difference.tv_nsec, data.counter);

   exit(signum);
}

int main(int argc, char *argv[]){
    signal(SIGINT, signal_callback_handler);

    sem_t *sem_ping = sem_open(SNAME_ping, 0); /* Open a preexisting semaphore. */
    if (sem_ping < 0)
    {
        perror("In sem_ping shm_open()");
        exit(1);
    }
    sem_t *sem_pong = sem_open(SNAME_pong, 0); /* Open a preexisting semaphore. */
    if (sem_pong < 0)
    {
        perror("In sem_pong shm_open()");
        exit(1);
    }
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &data.start);
    while(1){
        sem_wait(sem_ping);
        ++data.counter;
        #ifdef DEBUG
            printf("pong\n");
        #endif
        sem_post(sem_pong);
    }
    return 0;
}