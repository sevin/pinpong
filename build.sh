gcc -o pong -Wall pong.c -lpthread && gcc -o ping -Wall ping.c -lpthread
gcc -o pong -Wall pong_sharedmutex.c -lpthread && gcc -o ping -Wall ping_sharedmutex.c -lpthread
gcc -O3 -o pong -Wall pong_sharedmutex.c -lpthread -lrt && gcc -O3 -o ping -Wall ping_sharedmutex.c -lpthread -lrt
