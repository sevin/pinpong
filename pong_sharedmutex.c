#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

struct shared_mutex
{
    pthread_cond_t cond;
    pthread_mutex_t mutex;
    long iterations;
};

struct s_data
{
    long counter;
    struct timespec start;
    long first_iteration;
    long iterations;
} data;

struct timespec diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

void signal_callback_handler(int signum)
{
    struct timespec time1;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    // Terminate program
    struct timespec difference = diff(data.start, time1);

    printf("Time taken %ld seconds %ld nanoseconds (%ld iterations and %ld counted)\n", 
    difference.tv_sec, difference.tv_nsec, data.iterations - data.first_iteration, data.counter);

    exit(signum);
}

int main(int argc, char *argv[])
{
    signal(SIGINT, signal_callback_handler);

    pthread_cond_t *condA;
    pthread_mutex_t *mutexA;
    pthread_cond_t *condB;
    pthread_mutex_t *mutexB;
    long *iterations;

    key_t key = 0Xc0fe0;
    char *shm;
    int shmid[2];

    /*
     * Create the segment.
     */
    if ((shmid[0] = shmget(key, sizeof(struct shared_mutex), 0666)) < 0)
    {
        perror("shmget");
        exit(1);
    }
    if ((shmid[1] = shmget(key+1, sizeof(struct shared_mutex), 0666)) < 0)
    {
        perror("shmget");
        exit(1);
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((shm = shmat(shmid[0], NULL, 0)) == (char *)-1)
    {
        perror("shmat");
        exit(1);
    }

    condA = (pthread_cond_t *)shm;
    mutexA = (pthread_mutex_t *)(shm + sizeof(pthread_cond_t));
    iterations = (long *)(mutexA + sizeof(pthread_mutex_t));

    if ((shm = shmat(shmid[1], NULL, 0)) == (char *)-1)
    {
        perror("shmat");
        exit(1);
    }

    condB = (pthread_cond_t *)shm;
    mutexB = (pthread_mutex_t *)(shm + sizeof(pthread_cond_t));
    
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &data.start);
    data.first_iteration = *iterations;
    while (1)
    {
        /* first, lock the mutex */
        pthread_mutex_lock(mutexA);
        pthread_cond_signal(condB);
        /* mutex is now locked - wait on the condition variable.             */
        /* During the execution of pthread_cond_wait, the mutex is unlocked. */
        pthread_cond_wait(condA, mutexA);
        
        // pthread_mutex_lock(mutexB);
        data.iterations = *iterations;
        #ifdef DEBUG
            printf("pong\n");
        #endif

        /* finally, unlock the mutex */ 
        pthread_mutex_unlock(mutexA);
        

        // pthread_mutex_unlock(mutexB);
        
        ++data.counter;
        // pthread_cond_signal(condB);
        
    }
    return 0;
}