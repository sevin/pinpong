#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>

/**
 * Time taken 3 seconds 50189094 nanoseconds (419517 iterations and 419517 counted)
 * 7.270716309470177 us
 * 
 */
struct shared_mutex
{
    pthread_cond_t cond;
    pthread_mutex_t mutex;
    long iterations;
};

int shmid[2];

void signal_callback_handler(int signum)
{
    // Terminate program
    /* Deallocate the shared memory segment.  */
    shmctl (shmid[0], IPC_RMID, 0);
    shmctl (shmid[1], IPC_RMID, 0);
    exit(signum);
}

int main(int argc, char *argv[])
{

    signal(SIGINT, signal_callback_handler);

    pthread_condattr_t condattr;
    pthread_cond_t *condA;
    pthread_cond_t *condB;
    pthread_mutexattr_t mutexattr;
    pthread_mutex_t *mutexA;
    pthread_mutex_t *mutexB;
    long *iterations;
    struct timeval  now;            /* time when we started waiting        */
    struct timespec timeout;        /* timeout value for the wait function */

    key_t key = 0Xc0fe0;
    char *shm;

    /*
     * Create the segment.
     */
    if ((shmid[0] = shmget(key, sizeof(struct shared_mutex), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    if ((shmid[1] = shmget(key+1, sizeof(struct shared_mutex), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((shm = shmat(shmid[0], NULL, 0)) == (char *) -1) {
        perror("shmat");
        exit(1);
    }

    condA = (pthread_cond_t *)shm;
    mutexA = (pthread_mutex_t *)(shm + sizeof(pthread_cond_t));
    iterations = (long*)(mutexA + sizeof(pthread_mutex_t));

    if ((shm = shmat(shmid[1], NULL, 0)) == (char *) -1) {
        perror("shmat");
        exit(1);
    }

    condB = (pthread_cond_t *)shm;
    mutexB = (pthread_mutex_t *)(shm + sizeof(pthread_cond_t));

    /* Set up condition variable attributes */
    pthread_condattr_init(&condattr);
    pthread_condattr_setpshared(&condattr, PTHREAD_PROCESS_SHARED);
    pthread_cond_init(condA, &condattr);
    pthread_cond_init(condB, &condattr);
    pthread_condattr_destroy(&condattr);

    /* Set up mutex variable attributes */
    pthread_mutexattr_init(&mutexattr);
    pthread_mutexattr_setpshared(&mutexattr, PTHREAD_PROCESS_SHARED);
    pthread_mutex_init(mutexA, &mutexattr);
    pthread_mutex_init(mutexB, &mutexattr);
    pthread_mutexattr_destroy(&mutexattr);

    *iterations=0;

    while (1)
    {      
        pthread_mutex_lock(mutexA);
        pthread_cond_signal(condA);
        pthread_cond_wait(condB, mutexA);
        ++(*iterations);
        #ifdef DEBUG
        printf("ping\n");
        #endif
        pthread_mutex_unlock(mutexA);
        

        // pthread_mutex_lock(mutexB);
        // /* get current time */ 
        // gettimeofday(&now);
        // /* prepare timeout value */
        // timeout.tv_sec = now.tv_sec;
        // timeout.tv_nsec = (now.tv_usec+1) * 1000; /* timeval uses microseconds.         */
        //                                     /* timespec uses nanoseconds.         */
        //                                     /* 1 nanosecond = 1000 micro seconds. */
        // if(pthread_cond_timedwait(condB, mutexB, &timeout) == ETIMEDOUT){
            // pthread_cond_wait(condB, mutexB);
            // #ifdef DEBUG
            // printf("pong didn't reveived\n");            
            // #endif
        // }
        // pthread_mutex_unlock(mutexB);
    }
    return 0;
}