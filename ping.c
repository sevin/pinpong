#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#define SNAME_ping "/mysem"
#define SNAME_pong "/mysem2"
/** 
 * Time taken 5 seconds 562700954 nanoseconds (830144 iterations)
 * 6.7008867786793616 us
 */
void signal_callback_handler(int signum)
{

        /**
         * Semaphore unlink: Remove a named semaphore  from the system.
         */
        if ( sem_unlink(SNAME_ping) < 0 )
        {
                perror("sem_unlink");
        }

        if ( sem_unlink(SNAME_pong) < 0 )
        {
                perror("sem_unlink");
        }

   // Terminate program
   exit(signum);
}

int main(int argc, char *argv[]){
    signal(SIGINT, signal_callback_handler);
    
    sem_t *sem_ping = sem_open(SNAME_ping, O_CREAT, S_IRUSR | S_IWUSR, 1); /* Initial value is 1. */
    if (sem_ping < 0)
    {
        perror("In sem_ping shm_open()");
        exit(1);
    }
    sem_t *sem_pong = sem_open(SNAME_pong, O_CREAT, S_IRUSR | S_IWUSR, 0); /* Initial value is 0. */
    if (sem_pong < 0)
    {
        perror("In sem_pong shm_open()");
        exit(1);
    }

    while(1){
        sem_wait(sem_pong);
        #ifdef DEBUG
            printf("ping\n");
        #endif
        sem_post(sem_ping);
    }
    return 0;
}